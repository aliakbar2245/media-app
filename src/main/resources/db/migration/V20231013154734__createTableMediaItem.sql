CREATE TABLE public.media_items (
	id bigserial NOT NULL,
	title varchar(255) NOT NULL,
	description text NULL,
	mime_type varchar(255) NOT NULL,
	media_location varchar(255) NOT NULL,
	created_date timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
	created_by varchar(255) NOT NULL,
	updated_date timestamptz NOT NULL,
	updated_by varchar(255) NOT NULL,
	deleted_date timestamptz NULL,
	version int NOT NULL DEFAULT 0,
	user_id varchar(255) NOT NULL
);