package com.api.media.mediaapp.common;

import com.api.media.mediaapp.entity.SessionUser;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Header;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Slf4j
@Component
public class JwtTokenUtil implements Serializable {
	public static final long JWT_TOKEN_VALIDITY = 24*60*60;
	@Value("${jwt.secret}")
	private String secret;


	public SessionUser extractAndValidateToken(String token) {
		try {
			Function<Claims, String> claimEmail = Claims::getSubject;
			Function<Claims, Date> claimExpiration = Claims::getExpiration;
			Claims claims = Jwts.parser()
					.setSigningKey(secret.getBytes())
					.parseClaimsJws(token)
					.getBody();
			SessionUser user = new SessionUser();
			user.setUserName(claimEmail.apply(claims));
			Date expiration = claimExpiration.apply(claims);
			user.setTokenExpired(expiration);
			return expiration.after(new Date())?user:null;

		} catch (Exception e) {
			log.error("error extract and validate the token: " + e.getMessage());
			return null;
		}
	}
}
