package com.api.media.mediaapp.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GlobalEntityResponse<T> {
    private String status;
    private Integer code;
    private T data;
    private String message;
    private Object pagination;

    public static <T> GlobalEntityResponse<T> ok(T data) {
        GlobalEntityResponse<T> response = new GlobalEntityResponse<>();
        response.code = HttpStatus.OK.value();
        response.message = "success";
        response.status = "" + HttpStatus.OK;
        response.data = data;
        return response;
    }

    public static <T> GlobalEntityResponse<T> nok(T data, HttpStatus httpStatus, Exception exception) {
        GlobalEntityResponse<T> response = new GlobalEntityResponse<>();
        response.code = httpStatus.value();
        response.message = exception.getMessage();
        response.status = "" + httpStatus;
        response.data = data;
        return response;
    }
}
