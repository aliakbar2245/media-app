package com.api.media.mediaapp;

import io.github.cdimascio.dotenv.Dotenv;
import io.github.cdimascio.dotenv.DotenvEntry;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.core.env.StandardEnvironment;

import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@SpringBootApplication
@ComponentScan(basePackages = "com.api.media.mediaapp")
public class MediaAppApplication {

	public static void main(String[] args) {
		try {
			Map<String, Object> dotenv = Dotenv.configure().load().entries()
					.stream().collect(Collectors.toMap(DotenvEntry::getKey, DotenvEntry::getValue));
			new SpringApplicationBuilder(MediaAppApplication.class)
					.environment(new StandardEnvironment() {
						@Override
						protected void customizePropertySources(MutablePropertySources propertySources) {
							super.customizePropertySources(propertySources);
							propertySources.addLast(new MapPropertySource("dotenvProperties", dotenv));
						}
					}).run(args);
		} catch (Exception e) {
			log.info("no .env file found, using server's environment variable");
			SpringApplication.run(MediaAppApplication.class, args);
		}
	}

}
