package com.api.media.mediaapp.configuration;

import com.api.media.mediaapp.adapter.app.service.MediaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import java.util.Optional;

@Configuration
@EnableJpaAuditing(auditorAwareRef = "auditorProvider")
public class JpaAuditingConfiguration {

  @Autowired
  private MediaService mediaUserInfoService;

  @Bean
  public AuditorAware<String> auditorProvider() {
    return () -> {
      String updateBy = "SYSTEM";
      if (mediaUserInfoService.getUserInfo().isPresent()) {
        updateBy = mediaUserInfoService.getUserInfo().get().getUserName();
      }
      return Optional.of(updateBy);
    };
  }
}
