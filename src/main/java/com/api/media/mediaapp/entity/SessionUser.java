package com.api.media.mediaapp.entity;

import lombok.Data;

import java.util.Date;

@Data
public class SessionUser {
    private String userName;
    private Date tokenExpired;
}
