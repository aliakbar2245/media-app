package com.api.media.mediaapp.adapter.app;

import com.api.media.mediaapp.adapter.app.in.CreateMediaRequestApp;
import com.api.media.mediaapp.adapter.app.repository.MediaRepository;
import com.api.media.mediaapp.adapter.app.service.MediaService;
import com.api.media.mediaapp.entity.MediaItem;
import com.api.media.mediaapp.entity.SessionUser;
import com.api.media.mediaapp.exception.BadRequestException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.tika.Tika;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Optional;


@Slf4j
@Service
public class MediaServiceImpl implements MediaService {
    @Autowired
    private Tika tika;
    @Value("${MEDIA_APP_LOC}")
    private String folderLocation;
    @Autowired
    private MediaRepository mediaRepository;

    @Override
    public Optional<SessionUser> getUserInfo() {
        try {
            SessionUser sessionUser = null;
            String userNameSession = SecurityContextHolder.getContext().getAuthentication().getName();
            if(!"anonymousUser".equalsIgnoreCase(userNameSession)){
                sessionUser.setUserName(userNameSession);
            }
            return Optional.of(sessionUser);
        } catch (Exception e){
            return Optional.empty();
        }
    }

    @Override
    @Transactional
    public void store(CreateMediaRequestApp command) throws BadRequestException, IOException {
        //generate file
        String rootProjectDir = System.getProperty("user.dir");
        File uploadDirectory = new File(folderLocation);
        if (!uploadDirectory.exists()) {
            uploadDirectory.mkdirs();
        }
        String fileName = StringUtils.isNotEmpty(command.getFileName())?command.getFileName():command.getFile().getOriginalFilename();
        String fileExtention = FilenameUtils.getExtension(command.getFile().getOriginalFilename());
        String fileFullLocation = rootProjectDir+File.separator+folderLocation+File.separator+fileName+"."+fileExtention;
        File file = convertMultipartToFile(command.getFile(), fileFullLocation);

        //detect media type
        String mimeType = null;
        try {
            mimeType = tika.detect(file);
        } catch(IOException ioe) {
            throw new BadRequestException("Fail to detect file type");
        }

        //store to database
        MediaItem newMedia = new MediaItem();
        newMedia.setTitle(fileName);
        newMedia.setMimeType(mimeType);
        newMedia.setDescription("Media Uploaded By User");
        newMedia.setMediaLocation(fileFullLocation);
        newMedia.setUserId(command.getUserId());
        mediaRepository.save(newMedia);
    }

    private File convertMultipartToFile(MultipartFile multipartFile, String fullpath) throws IOException{
        File convertedFile = new File(fullpath);
        FileOutputStream fileOutputStream = new FileOutputStream(convertedFile);
        fileOutputStream.write(multipartFile.getBytes());
        fileOutputStream.close();
        return convertedFile;
    }
}
