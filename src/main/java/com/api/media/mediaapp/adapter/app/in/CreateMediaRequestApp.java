package com.api.media.mediaapp.adapter.app.in;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Setter
@Getter
@Builder
public class CreateMediaRequestApp {
    @NotEmpty(message = "userId is required")
    private String userId;
    @NotEmpty(message = "fileName is required")
    private String fileName;
    @NotNull(message = "file is required")
    private MultipartFile file;
}
