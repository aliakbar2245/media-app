package com.api.media.mediaapp.adapter.app.repository;

import com.api.media.mediaapp.entity.MediaItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MediaRepository extends JpaRepository<MediaItem, Long> {
}
