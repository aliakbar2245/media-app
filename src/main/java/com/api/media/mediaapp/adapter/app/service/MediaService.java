package com.api.media.mediaapp.adapter.app.service;

import com.api.media.mediaapp.adapter.app.in.CreateMediaRequestApp;
import com.api.media.mediaapp.entity.SessionUser;
import com.api.media.mediaapp.exception.BadRequestException;

import java.io.IOException;
import java.util.Optional;

public interface MediaService {
    Optional<SessionUser> getUserInfo();
    void store(CreateMediaRequestApp command) throws BadRequestException, IOException;
}
