package com.api.media.mediaapp.adapter.web;

import com.api.media.mediaapp.adapter.app.in.CreateMediaRequestApp;
import com.api.media.mediaapp.adapter.app.service.MediaService;
import com.api.media.mediaapp.common.GlobalEntityResponse;
import com.api.media.mediaapp.exception.BadRequestException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Slf4j
@RestController
@RequestMapping(value = "/v1/api/media")
public class MediaController {

    @Autowired
    private MediaService mediaService;

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public GlobalEntityResponse<String> storeMedia(@RequestPart(value = "userId") String userId,
                                                   @RequestPart(value = "fileName") String fileName,
                                                   @RequestPart(value = "file") MultipartFile file)
            throws BadRequestException, IOException {
        //handle if userId empty
        if(StringUtils.isEmpty(userId)){
            throw new BadRequestException("[BAD REQUEST] userId is required");
        }
        //handle if fileName empty
        if(StringUtils.isEmpty(fileName)){
            throw new BadRequestException("[BAD REQUEST] fileName is required");
        }
        //handle if file empty
        if(file.isEmpty()){
            throw new BadRequestException("[BAD REQUEST] file is required");
        }
        mediaService.store(CreateMediaRequestApp.builder()
                        .userId(userId)
                        .fileName(fileName)
                        .file(file)
                .build());
        return GlobalEntityResponse.ok("uploaded file success!");
    }
}
