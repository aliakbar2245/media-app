FROM openjdk:8-jdk-alpine
COPY target/media-app-0.0.1-SNAPSHOT.jar mediaapp.jar
EXPOSE 8787
ENTRYPOINT ["java","-jar","mediaapp.jar"]
