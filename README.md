# Getting Started

For further reference, please consider the following sections:

1. In this project, using docker as a containerization. Please download [here to Docker Desktop](https://www.docker.com/products/docker-desktop/) and install
2. Change the `.env.template` to `.env` in the root project

# Installation

Provide step-by-step instructions on how to install and run your project. Include any prerequisites and dependencies.

1. Make sure your docker installation according to your OS and running well. Example in windows section :
   - Docker running in taskbar ![docker_running_windows](md_image/docker_running_windows.PNG)<br/><br/>
   - Docker list image ![docker_image_windows](md_image/docker_image_windows.PNG)<br/><br/>
2. Type in terminal `docker compose up --build mediadb -d` wait for it until done.<br/>`mediadb` is a database service name define in `docker-compose.yml`
3. Type in terminal `docker compose up --build mediaapp -d` wait for it until done.<br/>`mediaapp` is a spring-boot application name define in `docker-compose.yml`
4. Done

# Security Api

In this project has `org.springframework.security` to secure the API fuction filter. So, to call the API you need the _**Header**_ `Authorization` mention/tag in process like below :

1. Sample `GET` format :

   - Payload :

   ```
    GET /v1/api/media/hello/{parameter} HTTP/1.1
    Host: localhost:8787
    Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhYUBnbWFpbC5jb20iLCJpYXQiOjE2OTcwODg2NDQsImV4cCI6MTY5NzE3NTA0NH0.ULMLAL32CLUhqf8YkEcVbxOJ6EHepq3qKtO3FhnOasJc9GQX3GNDxHd2uncB9nRK3rgMHUGVmE6CBHDuECUM5Q
   ```

   - Reject Status (403 Forbidden): When token was invalid ![Forbidden](md_image/token_invalid.PNG)<br/><br/>
   - OK Status (200 OK): When token was success in validation ![OK](md_image/token_validation_success.PNG)

2) etc
